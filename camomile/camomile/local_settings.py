# coding: utf-8
LOCAL_SETTINGS = True
from settings import *

DEBUG = True

DEFAULT_DOMAIN = 'localhost:8000'

EMAIL_DEBUG = DEBUG
CONTACT_EMAIL = 'camomile@the7bits.com'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
DEFAULT_FROM_EMAIL = 'no-reply@the7bits.com'