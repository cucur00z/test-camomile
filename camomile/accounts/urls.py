from django.conf.urls import patterns, include, url
from django.contrib.auth.views import login
from django.views.generic import TemplateView
from views import InviteCreate

urlpatterns = patterns('',
    url(r'^login/$', login, {'template_name': 'accounts/login.html'}),
    url(r'^request-invite/$', InviteCreate.as_view(), name='request_invite'),
    url(r'^request-invite/success/$', TemplateView.as_view(template_name="accounts/invite_success.html")),
)