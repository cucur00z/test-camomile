from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from models import Customer, Invite
from forms import CustomerChangeForm, CustomerCreationForm

# Register your models here.

class CustomerAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'nickname', 'password', 'activation_code')}),
        (_('Permissions'), {'fields': ('is_active', 'is_admin', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'fields': ('email', 'nickname', 'password1', 'password2'),
            'classes': ('wide',),
        }),
    )

    list_display = ('nickname', 'email', 'is_admin',)
    list_filter = ('is_admin', 'is_superuser', 'is_active', 'groups',)
    search_fields = ('nickname', 'email',)
    ordering = ('nickname',)
    filter_horizontal = ('groups', 'user_permissions',)

    form = CustomerChangeForm

    add_form = CustomerCreationForm
    add_form_template = 'accounts/admin/auth/user/add_form.html'


class InviteAdmin(admin.ModelAdmin):
    pass

admin.site.register(Customer, CustomerAdmin)
admin.site.register(Invite, InviteAdmin)

