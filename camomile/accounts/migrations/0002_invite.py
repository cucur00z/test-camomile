# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Invite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(unique=True, max_length=255, verbose_name='Email')),
                ('code', models.CharField(max_length=50, verbose_name='Code')),
                ('is_sent', models.BooleanField(default=False, verbose_name='Sent')),
                ('is_active', models.BooleanField(default=True, verbose_name='Active')),
                ('date_requested', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date requested')),
                ('date_used', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date used')),
                ('registered_customer', models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Invite',
                'verbose_name_plural': 'Invites',
            },
            bases=(models.Model,),
        ),
    ]
