from django.shortcuts import render
from django.views.generic.edit import CreateView
from forms import InviteCreationForm

# Create your views here.
class InviteCreate(CreateView):
    form_class = InviteCreationForm
    template_name = 'accounts/invite_new.html'
    success_url = 'success'