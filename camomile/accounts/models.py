from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone, six
from django.dispatch import receiver
from django.db.models.signals import post_save


ALREADY_ACTIVATED = 'ALREADY_ACTIVATED'

class CustomerManager(BaseUserManager):
    """
    Creates & saves a Customer with the given email, nickname and password
    """
    def create_user(self, email, nickname, password=None):
        """
        Creates & saves a Customer with the given email, nickname and password
        :param email:
        :param nickname:
        :param password:
        :return:
        """
        if not email:
            raise ValueError(_('Customers must have an email address'))

        customer = self.model(
            email = CustomerManager.normalize_email(email),
            nickname = nickname
        )
        customer.set_password(password)
        customer.save(using=self._db)
        return customer

    def create_superuser(self, email, nickname, password):
        """
        Creates & saves a superuser with the given email, nickname and password
        :param email:
        :param nickname:
        :param password:
        :return:
        """
        customer = self.create_user(email, nickname, password)
        customer.is_admin = True
        customer.save(using=self._db)


class Customer(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
        _('Email'),
        max_length=255,
        unique=True
    )
    nickname = models.CharField(
        _('Nickname'),
        max_length=50,
        unique=True
    )
    is_admin = models.BooleanField(
        _('Admin status'),
        default=False
    )
    is_active = models.BooleanField(
        _('Active'),
        default=True
    )
    date_joined = models.DateTimeField(
        _('Date joined'),
        default=timezone.now
    )
    activation_code = models.CharField(
        _('Activation code'),
        max_length=255
    )

    objects = CustomerManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['nickname']

    class Meta:
        verbose_name = _('Customer')
        verbose_name_plural = _('Customers')

    def __str__(self):
        return self.nickname

    def get_full_name(self):
        return self.nickname

    def get_short_name(self):
        return self.nickname

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin


class Invite(models.Model):
    """
    A model which implements the invite record.
    Email field is used for sending the mail.
    """
    email = models.EmailField(
        _('Email'),
        max_length=255,
        unique=True
    )
    code = models.CharField(
        _('Code'),
        max_length=50
    )
    is_sent = models.BooleanField(
        _('Sent'),
        default=False
    )
    is_active = models.BooleanField(
        _('Active'),
        default=True
    )
    date_requested = models.DateTimeField(
        _('Date requested'),
        default=timezone.now
    )
    date_used = models.DateTimeField(
        _('Date used'),
        default=timezone.now
    )
    registered_customer = models.OneToOneField(
        Customer,
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = _('Invite')
        verbose_name_plural = _('Invites')

    def __str__(self):
        return six.text_type(self.email)

REQUEST_INVITE_TEXT = _('''
Thank you for your interest to our product.
We'll send you the invite as soon as possible.
''')

@receiver(post_save, sender=Invite)
def invite_save_handler(sender, instance, created, **kwards):
    if created:
        import string
        import random
        from mail.utils import send_mail

        size = 50
        allowed = string.ascii_letters
        randomstring = ''.join([allowed[random.randint(0, len(allowed) - 1)] for x in xrange(size)])
        instance.code = randomstring
        instance.save(update_fields=['code'])
        send_mail(to_emails=[instance.email],
                from_email=settings.DEFAULT_FROM_EMAIL,
                subject=_('Your request is added to queue'),
                data={'content':REQUEST_INVITE_TEXT})

